module.exports = {
  roots: ["<rootDir>"],
  testPathIgnorePatterns: [
    '/dist/', '/src/', '/coverage/'
  ],
  testRegex: '(__test__/.*|(\\.|/)(test|spec))\\.ts$',
  transform: {
    "^.+\\.(ts)$": "ts-jest",
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageReporters: ['html', 'lcov', 'json', 'text'],
  collectCoverage: true,
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 0,
      statements: 0
    }
  }
};
